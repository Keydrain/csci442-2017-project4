﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class Collide : MonoBehaviour {

	public static float timeLeft = 30;
	public Text timeText;
	public Text winText;
	public RawImage cross;
	private bool won = false;
	private bool lose = false;

	// Use this for initialization
	void Start () {
		timeText.text = "Time Left: " + Mathf.Round(timeLeft);
		winText.text = "";
	}

	// Update is called once per frame
	void Update () {
		if (!won && !lose) {
			timeLeft -= Time.deltaTime;
			timeText.text = "Time Left: " + Mathf.Round(timeLeft);
			if (timeLeft < 0) {
				if (cross != null) {
					Destroy (cross.gameObject);
				}
				winText.text = "You Lose!";
				lose = true;
			}
		}
	}

	void OnCollisionExit (Collision col)
	{
		if((col.gameObject.name == "CannonBall(Clone)" || col.gameObject.name == "CannonBall") && !lose)
		{
			if (cross != null) {
				Destroy (cross.gameObject);
			}
			won = true;
			winText.text = "You Win!";
		}
	}

	float getTimeLeft() {
		return timeLeft;
	}
}
