﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseDown : MonoBehaviour {

	public GameObject ball;
	public float power;
	public Text countText;
	public int count = 0;
	public Button yourButton;

	// Use this for initialization
	void Start () {
		Button btn = yourButton.GetComponent<Button>();
		btn.onClick.AddListener(TaskOnClick);
		countText.text = "CannonBalls: " + count;
	}
	
	// Update is called once per frame
	void FixedUpdate(){
		//if ((Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space)) && count > 0){
		if (Input.GetKeyDown(KeyCode.Space)){
			FireCannon ();
		}
	}

	void TaskOnClick(){
		FireCannon ();
	}

	void FireCannon(){
		if (count > 0) {
			GameObject CannonBall = GameObject.Instantiate (ball, new Vector3(transform.position.x, transform.position.y - 1, transform.position.z), transform.rotation) as GameObject;
			Rigidbody rb = CannonBall.GetComponent<Rigidbody> ();

			rb.AddForce(transform.forward * power + transform.up * power / 8);
			count--;
			countText.text = "CannonBalls: " + count;
		}
	}
}
