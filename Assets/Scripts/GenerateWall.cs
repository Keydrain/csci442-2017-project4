﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GenerateWall : MonoBehaviour {

	public GameObject[] blocks;
	public int Height;
	public int Width;
	private System.Random r = new System.Random();

	void Start () {

		for(int x = 1; x <= Width; x++ ){
			for(int y = 1; y <= Height; y++ ){
				int rNum = r.Next(0, blocks.Length);
				if (rNum == 0) {
					GameObject RedCube   = GameObject.Instantiate (blocks [0], new Vector3 (x - Width / 2.0f - 0.5f, y - 2.49f, 9), Quaternion.identity) as GameObject;
				} else if (rNum == 1) {
					GameObject GreenCube = GameObject.Instantiate (blocks [1], new Vector3 (x - Width / 2.0f - 0.5f, y - 2.49f, 9), Quaternion.identity) as GameObject;
				} else if (rNum == 2) {
					GameObject BlueCube  = GameObject.Instantiate (blocks [2], new Vector3 (x - Width / 2.0f - 0.5f, y - 2.49f, 9), Quaternion.identity) as GameObject;
				} else {
					print("Error Building Wall");
				}
			}
		}	
	}	
}
